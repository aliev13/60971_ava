<?php

use framework\Routing\Request;
use framework\Routing\Router;
require_once "vendor/autoload.php";

echo (new Router(new Request()))->getContent();